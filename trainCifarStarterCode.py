from scipy import misc
import numpy as np
import tensorflow as tf
import random
import matplotlib.pyplot as plt
import matplotlib as mp

# --------------------------------------------------
# setup

def weight_variable(shape):
    '''
    Initialize weights
    :param shape: shape of weights, e.g. [w, h ,Cin, Cout] where
    w: width of the filters
    h: height of the filters
    Cin: the number of the channels of the filters
    Cout: the number of filters
    :return: a tensor variable for weights with initial values
    '''

    # IMPLEMENT YOUR WEIGHT_VARIABLE HERE
    initial = tf.truncated_normal(shape, stddev=0.1)
    W = tf.Variable(initial)
    return W

def bias_variable(shape):
    '''
    Initialize biases
    :param shape: shape of biases, e.g. [Cout] where
    Cout: the number of filters
    :return: a tensor variable for biases with initial values
    '''

    # IMPLEMENT YOUR BIAS_VARIABLE HERE
    initial = tf.constant(0.1, shape=shape)
    b = tf.Variable(initial)
    return b

def conv2d(x, W):
    '''
    Perform 2-D convolution
    :param x: input tensor of size [N, W, H, Cin] where
    N: the number of images
    W: width of images
    H: height of images
    Cin: the number of channels of images
    :param W: weight tensor [w, h, Cin, Cout]
    w: width of the filters
    h: height of the filters
    Cin: the number of the channels of the filters = the number of channels of images
    Cout: the number of filters
    :return: a tensor of features extracted by the filters, a.k.a. the results after convolution
    '''

    # IMPLEMENT YOUR CONV2D HERE
    h_conv = tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')
    return h_conv

def max_pool_2x2(x):
    '''
    Perform non-overlapping 2-D maxpooling on 2x2 regions in the input data
    :param x: input data
    :return: the results of maxpooling (max-marginalized + downsampling)
    '''

    # IMPLEMENT YOUR MAX_POOL_2X2 HERE
    h_max = tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
    return h_max

def variable_summaries(var, name):
  """Attach a lot of summaries to a Tensor."""
  with tf.name_scope('summaries'):
    mean = tf.reduce_mean(var)
    tf.scalar_summary('mean/' + name, mean)
    with tf.name_scope('stddev'):
      stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
    tf.scalar_summary('sttdev/' + name, stddev)
    tf.scalar_summary('max/' + name, tf.reduce_max(var))
    tf.scalar_summary('min/' + name, tf.reduce_min(var))
    tf.histogram_summary(name, var)

def plotting():
    #plot train/test accuracy and loss
    plt.figure(1)
    plt.subplot(211)
    plt.plot(steps, train_accs, 'r-', steps, test_accs, 'b-')
    plt.ylabel('accuracy')
    plt.xlabel('steps')
    plt.subplot(212)
    plt.plot(steps, losses, 'b-')
    plt.ylabel('loss')
    plt.xlabel('steps')
    #plt.show()
    plt.savefig('plot.png')

ntrain = 100 # per class
ntest = 10 # per class
nclass = 10 # number of classes
imsize = 28
nchannels = 1	
batchsize = 100

Train = np.zeros((ntrain*nclass,imsize,imsize,nchannels))
Test = np.zeros((ntest*nclass,imsize,imsize,nchannels))
LTrain = np.zeros((ntrain*nclass,nclass))
LTest = np.zeros((ntest*nclass,nclass))

itrain = -1
itest = -1
for iclass in range(0, nclass):
    for isample in range(0, ntrain):
        path = '/home/gino/CIFAR10/Train/%d/Image%05d.png' % (iclass,isample)
        im = misc.imread(path); # 28 by 28
        im = im.astype(float)/255
        itrain += 1
        Train[itrain,:,:,0] = im
        LTrain[itrain,iclass] = 1 # 1-hot lable
    for isample in range(0, ntest):
        path = '/home/gino/CIFAR10/Test/%d/Image%05d.png' % (iclass,isample)
        im = misc.imread(path); # 28 by 28
        im = im.astype(float)/255
        itest += 1
        Test[itest,:,:,0] = im
        LTest[itest,iclass] = 1 # 1-hot lable

sess = tf.InteractiveSession()

tf_data = tf.placeholder(tf.float32, shape=[None, imsize, imsize, nchannels]) #tf variable for the data, remember shape is [None, width, height, numberOfChannels] 
tf_labels = tf.placeholder(tf.float32, shape=[None, 10]) #tf variable for labels

# --------------------------------------------------
# model
#create your model
# FILL IN THE CODE BELOW TO BUILD YOUR NETWORK


W = tf.Variable(tf.zeros([1,28,28,10]))
b = tf.Variable(tf.zeros([10]))

#y = tf.nn.softmax(tf.matmul(tf_data,W) + b)

# reshape the input image
#x_image = tf.reshape(tf_data, [-1, 28, 28, 1])

# first convolutional layer
W_conv1 = weight_variable([5, 5, 1, 32])
b_conv1 = bias_variable([32])
h_conv1 = tf.nn.sigmoid(conv2d(tf_data, W_conv1) + b_conv1)
h_pool1 = max_pool_2x2(h_conv1)

# second convolutional layer
W_conv2 = weight_variable([5, 5, 32, 64])
b_conv2 = bias_variable([64])
h_conv2 = tf.nn.sigmoid(conv2d(h_pool1, W_conv2) + b_conv2)
h_pool2 = max_pool_2x2(h_conv2)

# densely connected layer
W_fc1 = weight_variable([7 * 7 * 64, 1024])
b_fc1 = bias_variable([1024])
h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
h_fc1 = tf.nn.sigmoid(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

# dropout
keep_prob = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

# softmax
W_fc2 = weight_variable([1024, 10])
b_fc2 = bias_variable([10])
y_conv = tf.nn.softmax(tf.matmul(h_fc1_drop, W_fc2) + b_fc2)

# --------------------------------------------------
# loss
#set up the loss, optimization, evaluation, and accuracy

# setup training
#learning_rate = 0.001
cross_entropy = tf.reduce_mean(-tf.reduce_sum(tf_labels * tf.log(y_conv), reduction_indices=[1]))
optimizer = tf.train.AdamOptimizer(1e-3).minimize(cross_entropy)
#optimizer = tf.train.MomentumOptimizer(1e-3, 0.9,use_locking=False, name='Momentum', use_nesterov=True).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv,1), tf.argmax(tf_labels,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# --------------------------------------------------
#visualization
result_dir = './results/' 
# Add a scalar summary for the snapshot loss.
tf.scalar_summary('cross_entropy', cross_entropy)

tf.scalar_summary('accuracy', accuracy)

#W_conv1_reshape = tf.reshape(W_conv1, [1, 25, 25, 1])
#tf.image_summary('conv_1_weight', W_conv1)

#variable summaries
#weights, biases, activation after relu, activation after max-pooling
#variable_summaries(W_conv1, 'W_conv1')
#variable_summaries(W_conv2, 'W_conv2')
#variable_summaries(W_fc1, 'W_fc1')
#variable_summaries(W_fc2, 'W_fc2')
#variable_summaries(b_conv1, 'b_conv1')
#variable_summaries(b_conv2, 'b_conv2')
#variable_summaries(b_fc1, 'b_fc1')
#variable_summaries(b_fc2, 'b_fc2')
#variable_summaries(h_conv1, 'Activation after relu, conv1 layer')
#variable_summaries(h_conv2, 'Activation after relu, conv2 layer')
#variable_summaries(h_pool1, 'Activation after max-pooling, conv1 layer')
#variable_summaries(h_pool2, 'Activation after max-pooling, conv2 layer')


# Visualize the first convolution layer weights

L1_filter_size = 5
L1_num_filters = 32

V1 = tf.reshape(W_conv1, (L1_filter_size, L1_filter_size, L1_num_filters))

# Reorder so the filters are in the first dimension, x and y follow.
V1 = tf.transpose(V1, (2, 0, 1))

# Bring into shape expected by image_summary
V1 = tf.reshape(V1, (L1_num_filters, L1_filter_size, L1_filter_size, 1))

tf.image_summary("weights", V1, max_images=L1_num_filters)



# Build the summary operation based on the TF collection of Summaries.
summary_op = tf.merge_all_summaries()
# Instantiate a SummaryWriter to output summaries and the Graph.
summary_writer = tf.train.SummaryWriter(result_dir, sess.graph)

# optimization
sess.run(tf.initialize_all_variables())
batch_xs = np.zeros((batchsize, imsize, imsize, nchannels)) #setup as [batchsize, width, height, numberOfChannels] and use np.zeros()
batch_ys = np.zeros((batchsize, nclass)) #setup as [batchsize, the how many classes] 
max_step = 500
steps = []
train_accs = []
test_accs = []
losses = []
for i in range(max_step): # try a small iteration size once it works then continue
    nsamples = 1000
    perm = np.arange(nsamples)
    np.random.shuffle(perm)
    for j in range(batchsize):
        batch_xs[j,:,:,:] = Train[perm[j],:,:,:]
        batch_ys[j,:] = LTrain[perm[j],:]
    if i%10 == 0:
        #calculate train accuracy and print it
        # output the training accuracy every 10 iterations
        train_accuracy = accuracy.eval(feed_dict={
            tf_data:batch_xs, tf_labels:batch_ys, keep_prob: 0.5})
        #print("step %d, training accuracy %g"%(i, train_accuracy))
        test_accuracy = accuracy.eval(feed_dict={tf_data: Test, tf_labels: LTest, keep_prob: 1.0})
        loss = cross_entropy.eval(feed_dict={
            tf_data:batch_xs, tf_labels:batch_ys, keep_prob: 0.5})
        print("step %d, training accuracy %g, test accuracy %g, loss %g"%(i, train_accuracy, test_accuracy, loss))
        steps.append(i)
        train_accs.append(train_accuracy)
        test_accs.append(test_accuracy)
        losses.append(loss)
        summary_str = sess.run(summary_op, feed_dict={tf_data: batch_xs, tf_labels: batch_ys, keep_prob: 0.5})
        summary_writer.add_summary(summary_str, i)
        summary_writer.flush()
        plotting()
        

    optimizer.run(feed_dict={tf_data: Train, tf_labels: LTrain, keep_prob: 0.5}) # dropout only during training
    #summary_writer.add_summary(summary, i)

# --------------------------------------------------
# test

print("test accuracy %g"%accuracy.eval(feed_dict={tf_data: Test, tf_labels: LTest, keep_prob: 1.0}))



sess.close()



